<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Order;
use App\Models\Promo;
use App\Models\User;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class OrdersController extends Controller
{
    public function __construct()
    {
    }
    /**
     * Show main Order page for Customer
     *
     * @return View
     */
    public function index(): View
    {
        $user = auth()->user();
        // var_dump($user);exit();
        return view('order')->with(['auth_user' => $user, 'auth_deliveries' => !is_null($user) ? $user->deliveries : null]);
    }

    private function authCheck()
    {
        if (\Auth::check()) {
            $period = time() + (86400 * 30);
            $user_id = \Auth::user()->id;
            setcookie('user', $user_id, $period);
            $carts = Cart::where('user_id', $user_id)->get();
            $count = $carts->count();
        } else {
            $period = time() + (86400 * 30);
            $random_string = new Promo();
            $user_temp = $random_string->generateRandomString();

            if (!isset($_COOKIE['temp_user'])) {
                setcookie('temp_user', $user_temp, $period);
                $carts = Cart::where('user_id', $user_temp)->get();
                $count = $carts->count();
            } else {
                $carts = Cart::where('user_id', $_COOKIE['temp_user'])->get();
                $count = $carts->count();
            }
        }
        return $count;
    }

    public function saveApiData($payload)
    {
        $client = new Client();

        $response = $client->post('https://jedyphp.retailcrm.ru/api/v5/orders/create?apiKey=RacDhvIe4N1gRVNxcpN1qp0f1AH3VVhm', [
            'debug' => false,
            'form_params' => $payload,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'verify' => 'C:\wamp64\bin\php\php7.1.9\cacert.pem',
        ]);
        // echo $response->getStatusCode();
        if ($response->getStatusCode() == 201) {
            $message = 'success';
        } else {
            $message = 'error';
        }
        return $message;
        //var_dump($response->getHeader('content-type'));
        // 'application/json; charset=utf8'
        // echo $response->getBody();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $status = 'success';
        try {
            $data = $request->all();
            // var_dump($request->all());
            // echo Auth::check();
            // exit();
            $cart = Cart::findOrFail($data['cart_id']);
            $sum = $cart->sum();

            if ($data['promo_id'] > 0) {
                $promo = Promo::findOrFail($data['promo_id']);
                if ($promo->sale > 0) {
                    $sum *= (100 - $promo->sale) / 100;
                }
            }

            $order = Order::create([
                'cart_id' => $data['cart_id'],
                'delivery_id' => isset($data['delivery_id']) && !empty($data['delivery_id']) ? $data['delivery_id'] : null,
                'price' => $sum,
                'payment_method_id' => $data['payment_method_id'],
                'status_id' => 1,
            ]);

            // Attach this cart to order
            $attach = Cart::findOrFail($data['cart_id']);
            $attach->order_id = $order->id;
            $attach->save();

            if (!empty($data['card'])) {
                $order->greeting()->create([
                    'text' => $data['card'],
                ]);
            }

            $result = $order;
        } catch (\Illuminate\Database\QueryException $exception) {
            $result = $exception->errorInfo;
            $status = 'error';
        }

        return ['status' => $status, 'order' => $result];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id)
    {
        $item = Order::findOrFail($order_id);
        $item->delete();

        return redirect()->action('OrdersController@cart');
    }

    public function cart()
    {
        $count = $this->authCheck();
        $price = 0;
        // $carts = Cart::where('user_id', $user_id)->get();
        $carts = DB::table('carts')
            ->join('items', function ($join) {

                if (isset($_COOKIE['user'])) {
                    $user_id = $_COOKIE['user'];
                } else {
                    $user_id = $_COOKIE['temp_user'];
                }

                $join->on('carts.item_id', '=', 'items.id')
                    ->where('user_id', $user_id);
            })
            ->get();

        // var_dump($carts);
        foreach ($carts as $cart) {
            $price = $price + $cart->price;
        }

        $items = Item::where('extra', 1)->orderBy(DB::raw('RAND()'))->paginate(3);
        // var_dump($items);
        return view('front.cart', compact('carts', 'price', 'items', 'count'));
    }

    public function applyPromo($cart_id, $code)
    {
        $result = null;

        $promo = Promo::where('code', $code)->where('expires', '>', new DateTime())->first();

        $cart = Cart::findOrFail($cart_id);
        // echo ($cart->promo);
        // exit();
        if (!$cart->promo) {
            $cart->promo_id = $promo->id;
            $cart->save();
            $result = $promo;
        }

        return $result;
    }
}
