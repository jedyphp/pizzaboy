<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Delivery;
use App\Models\Order;
use App\Models\User;
use Faker\Factory as Faker;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function createDelivery(Request $request)
    {
        $data = $request->all();

        if ($data['order']) {
            $order = Order::findOrFail($data['order']);
        }

        $delivery = Delivery::create([
            'order_id' => $order->id,
            'city' => $data['city'],
            'street' => $data['street'],
            'building' => $data['building'],
            'suite' => $data['suite'],
        ]);

        $order->delivery_id = $delivery->id;
        $order->save();

        return ['success' => true];
    }

    public function defaultDelivery(Request $request)
    {
        $success = true;

        try {
            $user_deliveries = Delivery::where('user_id', $request->user_id)->get();

            foreach ($user_deliveries as $delivery) {
                $delivery->active = false;
                $delivery->save();
            }

            $default = Delivery::findOrFail($request->delivery_id);
            $default->active = true;
            $result = $default->save();
        } catch (\Illuminate\Database\QueryException $exception) {
            $result = $exception->errorInfo;
            $success = false;
        }

        return ['success' => $success, 'result' => $result];
    }

    public function profile()
    {
        if (Auth::check()) {
            $orders = Cart::with('order')->with('items')->where('user_id', Auth::user()->id)->whereNotNull('order_id')->get();
        }

        $user = User::findOrFail(Auth::user()->id);

        return view('cabin', compact('orders', 'user'));
    }

    protected static function calculateBonus()
    {
        $quantity = 0;

        $order_carts = Auth::user()->carts()->whereNotNull('order_id')->with(['items' => function ($q) {
            $q->where('extra', 0);
        }])->get();

        foreach ($order_carts as $cart) {
            foreach ($cart->items as $item) {
                $quantity += $item->pivot->quantity;
            }
        }

        return $quantity;
    }

}
