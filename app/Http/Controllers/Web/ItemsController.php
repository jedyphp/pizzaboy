<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ItemsController extends Controller
{

    public function __construct()
    {
        // $this->middleware(['auth', 'admin']);
    }

    /**
     * Get Items list
     *
     * @return \App\Models\Item[]
     */
    function list() {
        $items = Item::with('photos')
            ->where('active', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('catalogue', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:items',
            'description' => 'required',
            'price' => 'required',
            // 'path' => 'mimes:jpeg,png|max:10240',
        ]);
        $data = $request->only('name', 'description', 'external_id', 'composition', 'price', 'amount', 'hit', 'extra');

        $item = Item::create($data);
        $item->categories()->sync($request->get('categories'));

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $item = Item::with('photos')->where('id', $id)->first();

        return view('details', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if ($item->photo !== '') {
                $this->deletePhoto($item->photo);
            }

            $item->update($data);
            return redirect()->route('admin.items.edit', $id);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required',
                'photo' => 'mimes:jpeg,png|max:10240',
                'amount' => 'numeric|min:1',
            ]);

            $data = $request->only('name', 'external_id', 'composition', 'description', 'size', 'price', 'amount', 'hit', 'extra');

            if (count($request->get('categories')) > 0) {
                $item->categories()->sync($request->get('categories'));
            } else {
                // no category set, detach all
                $item->categories()->detach();
            }

            $item->update($data);
            return redirect()->action('ItemsController@index');
        }
    }
}
