<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Item;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CartsController extends Controller
{
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null): View
    {
        return view('cart');
    }

    public function get($cart_id)
    {
        $items = Cart::with('items')->where('id', $cart_id)->first()->items()->with('photos')->get();

        return ['items' => $items];

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): array
    {
        try {
            $user = $request->get('user');
            $cart_id = $request->get('cart_id');

            if (\Auth::check()) {
                $user_id = \Auth::id();
                $temp = null;
            } elseif (gettype($user) === "integer") {
                $user_id = $user;
                $temp = null;
            } else {
                $user_id = null;
                $temp = !empty($user) ? $user : $this->rand(8);
            }

            $data = [
                'user_id' => $user_id,
                'temp' => $temp,
            ];

            // If user is new guest -> create new cart
            // If user is returned guest -> attach new items to his cart
            // If user is logged use his permanent cart
            if (gettype($user) === "integer") {
                $cart = Cart::where('id', $cart_id)->where('user_id', $user_id)->first();
            } else {
                $cart = Cart::where('id', $cart_id)->where('temp', $temp)->first();
            }

            if (!$cart || $cart->order()->first()) {
                $cart = Cart::create($data);
            }

            $cart->items()->attach($request->get('item_id'));
            $success = true;

        } catch (\Illuminate\Database\QueryException $exception) {
            $cart = $exception->errorInfo;
            $success = false;
        }

        return ['success' => $success, 'data' => $cart];
    }

    public function webStore(Request $request)
    {
        $add = $this->store($request);
        if ($add['success']) {
            return view('cart');
        }
    }

    public function update(Request $request)
    {
        $success = true;

        $cart_id = $request->get('cart_id');
        $item_id = $request->get('item_id');
        $action = $request->get('action');

        try {
            $cart = Cart::find($cart_id);

            $item = $cart->items()->where('item_id', $item_id)->first();
            $quantity = $action === 'add' ? ++$item->pivot->quantity : --$item->pivot->quantity;
            $item->pivot->quantity = $quantity;
            $result = $item->pivot->save();

        } catch (\Illuminate\Database\QueryException $exception) {
            $result = $exception->errorInfo;
            $success = false;
        }

        return ['success' => $success, 'saved' => $result];
    }

    public function removeItem($id, $item_id)
    {
        $success = true;

        try {
            $cart = Cart::findOrFail($id);
            $result = $cart->items()->detach($item_id);

        } catch (\Illuminate\Database\QueryException $exception) {
            $result = $exception->errorInfo;
            $success = false;
        }

        return ['success' => $success, 'result' => $result];
    }

    public function rand($length)
    {
        $include_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charLength = strlen($include_chars);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $include_chars[rand(0, $charLength - 1)];
        }
        return $randomString;
    }

}
