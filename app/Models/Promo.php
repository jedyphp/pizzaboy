<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $fillable = ['code', 'sale', 'expires'];

    public $timestamps = false;

    /**
     * Get the Cart for this Promo.
     */
    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }
}
