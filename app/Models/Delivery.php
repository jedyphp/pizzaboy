<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $fillable = ['user_id', 'street', 'city', 'building', 'suite', 'name', 'active', 'order_id'];
    protected $hidden = ['user_id', 'name'];

    public $timestamps = false;

    /**
     * Get the User for this Delivery.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }

}
