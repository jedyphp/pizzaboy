<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    protected $fillable = ['item_id', 'user_id', 'temp', 'order_id'];

    public static function boot()
    {
        parent::boot();
        // Можно прописать удаление Cart при удалении User
    }

    /**
     * Get the User for this Cart.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the Promo for this Cart.
     */
    public function promo()
    {
        return $this->belongsTo('App\Models\Promo');
    }

    /**
     * Get the Items for this Cart.
     */
    public function items()
    {
        return $this->belongsToMany('App\Models\Item')->withPivot('quantity');
    }

    /**
     * Get the Items for this Cart.
     */
    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }

    public function countItems()
    {
        return $this->belongsToMany('App\Models\Item')
            ->select('*', DB::raw('count(item_id) as quantity'))
            ->groupBy('item_id');
    }

    public function sum()
    {
        $sum = 0;

        $items = $this->items()->get();
        foreach ($items as $item) {
            $sum += $item->price * $item->pivot->quantity;
        }

        return $sum;
    }

}
