<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'description', 'price', 'path', 'is_active'];

    /**
     * Get the Photo photos for this Items.
     */
    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }
}
