<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['item_id', 'path'];
    protected $appends = ['path'];

    public $timestamps = false;

    /**
     * Get the Item for this Photo.
     */
    public function item()
    {
        return $this->belongsTo('App\Model\Item');
    }

    public function getPathAttribute()
    {
        return '/storage/'.  $this->attributes['path'];
    }
}
