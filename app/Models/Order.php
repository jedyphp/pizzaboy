<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['cart_id', 'delivery_id', 'price', 'payment_method_id', 'promo_id', 'bonus_id', 'status_id', 'greeting_id'];

    public $timestamps = true;

    /**
     * Get the Cart for this Order.
     */
    public function cart()
    {
        return $this->belongsTo('App\Models\Cart');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->setTimezone('Europe/Moscow')->format('Y-m-d H:i');
    }

    /**
     * Get the Promo for this Order.
     */
    public function promo()
    {
        return $this->belongsTo('App\Models\Promo');
    }

    /**
     * Get the Payment Method for this Order.
     */
    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod');
    }

    /**
     * Get the Delivery for this Order.
     */
    public function delivery()
    {
        return $this->belongsTo('App\Models\Delivery');
    }

    /**
     * Get the Greeting for this Order.
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }
}
