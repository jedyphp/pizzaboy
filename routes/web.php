<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::view('login', 'login')->name('login');
Route::view('register', 'register')->name('register');

Route::group(['namespace' => 'Web'], function () {

    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', 'CartsController@index');
        Route::get('/{cart_id}', 'CartsController@get');
        Route::get('/{cart_id}/free', 'CartsController@free');

        Route::post('/', 'CartsController@store');
        Route::post('/web', 'CartsController@webStore');
        Route::post('/update', 'CartsController@update');
        Route::delete('/{id}/{item_id}', 'CartsController@removeItem');
    });

    Route::group(['prefix' => 'order'], function () {
        Route::get('/', 'OrdersController@index');
        Route::post('/', 'OrdersController@store');
    });

    Route::group(['prefix' => 'items'], function () {
        Route::get('/list', 'ItemsController@list');
        Route::post('/filter', 'ItemsController@filter');
        Route::get('/{item_id}', 'ItemsController@details');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@profile');
        Route::post('delivery', 'UserController@createDelivery');
        Route::post('delivery/default', 'UserController@defaultDelivery');
    });

    Route::get('payment_methods', 'PaymentMethodController@get');
    Route::get('promo/{cart_id}/{promo}', 'OrdersController@applyPromo');

    Route::get('/', 'ItemsController@list')->name('catalogue');
    Route::get('about', 'PagesController@about')->name('about');
    Route::get('delivery', 'PagesController@delivery')->name('delivery');
});

Route::group(['namespace' => 'Auth'], function () {
    Route::post('login', 'LoginController@login');
    Route::post('register', 'RegisterController@register');
});
