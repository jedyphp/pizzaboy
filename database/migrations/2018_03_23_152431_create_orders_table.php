<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('cart_id')->unsigned()->nullable();
            $table->integer('delivery_id')->unsigned()->nullable();
            $table->float('price')->unsigned();
            $table->integer('promo_id')->nullable();
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->bigInteger('status_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('orders', function($table) {
            $table->foreign('cart_id')->references('id')->on('carts')->onDelete('set null');
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('orders');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
