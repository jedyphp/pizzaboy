<?php

use Illuminate\Database\Seeder;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'new',
            'cancel',
            'pending',
            'paid',
            'progress',
            'done',
        ];

        foreach ($statuses as $status) {
            DB::table('statuses')->insert([
                'status' => $status,
            ]);
        }
    }
}
