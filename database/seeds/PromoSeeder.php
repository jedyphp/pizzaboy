<?php

use App\Models\Promo;
use Illuminate\Database\Seeder;

class PromoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promos = [
            'promo',
        ];

        $date = new DateTime();
        date_add($date, date_interval_create_from_date_string('10 days'));

        foreach ($promos as $promo) {
            Promo::create([
                'sale' => 15,
                'code' => $promo,
                'expires' => $date,
            ]);
        }

    }
}
