<?php

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_methods = [
            'Credit card',
            'Cash',
        ];

        foreach ($payment_methods as $method) {
            PaymentMethod::create([
                'payment_method' => $method,
            ]);
        }
    }
}
