Vue.component(
    "view-slider",
    require("./components/view-slider.vue").default
);

Vue.component(
    "view-details",
    require("./components/view-details.vue").default
);

Vue.component(
    "catalogue",
    require("./components/catalogue.vue").default
);


Vue.component(
    "cart-component",
    require("./components/cart.vue").default
);

Vue.component(
    "order-component",
    require("./components/order.vue").default
);

Vue.component(
    "cabin-component",
    require("./components/cabin.vue").default
);

Vue.component(
    "modal",
    require("./components/modal.vue").default
);

Vue.component(
    "web-login",
    require("./components/web.login.vue").default
);

Vue.component(
    "web-register",
    require("./components/web.register.vue").default
);

Vue.component(
    "info-message",
    require("./components/info-message.vue").default
);

import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';

Vue.component('vue-phone-number-input', VuePhoneNumberInput);

import VueInputUi from 'vue-input-ui';
import 'vue-input-ui/dist/vue-input-ui.css';

Vue.component('vue-input', VueInputUi);

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);
