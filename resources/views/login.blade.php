<html>
<head>
    <title>PizzaBoy - Login</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta property="og:url" content="https://pizzaboy-testing.com" />
    <meta property="og:title" content="pizzaboy-testing.com | Закажи на сайте, забери в мастерской" />
    <meta property="og:description" content="Невероятно быстрая доставка по городу. Выбери и оформи заказ прямо на сайте!" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="asset('images/og2.png')" />
</head>
<body>
    <div class="wrapper" id="app">
        @include('partials/_header')
        <div class="content">
            <div class="text-center mt-4 font-primary">
                <h1>Login</h1>
            </div>

            <div class="flex-center mt-4">
                <div class="col-lg-3 text-center">
                    <form method="POST" action="{{ url('login') }}">

                        {{ csrf_field() }}

                        <web-login></web-login>

                        @if($errors->any())
                        <span class="text-danger font-primary">{{$errors->first()}}</span>
                        @endif

                        <div class="form-group flex-center pt-3">
                            <button class="order-button px-4">Login</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        @include('partials/_footer')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
