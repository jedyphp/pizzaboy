@extends('layout')
@section('title', 'Pizzaboy - Пицца в каждый дом')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы в Таганроге ! Успейте заказать! 8 800 200 59 10" />
<meta name="keywords" content="Pizzaboy - Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы ! Успейте заказать! 8 800 200 59 10" />
<link rel="canonical" href="https://pizzaboy-testing.com/">
@endsection
@section('facebook_meta')
<meta property="og:url" content="https://pizzaboy-testing.com" />
<meta property="og:title" content="Pizzaboy | Бонусы при заказе на сайте" />
<meta property="og:description" content="Заказывать пиццу онлайн удобно! Выбери и оформи заказ прямо на сайте!" />
<meta property="og:type" content="website" />
<meta property="og:image" content="asset('images/og2.png')" />
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title mt-2">
            <h1>Hits of current day</h1>
        </div>
    </div>
</div>

<catalogue :load="{{ $items }}"></catalogue>
@endsection
