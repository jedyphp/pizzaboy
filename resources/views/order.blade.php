@extends('layout')
@section('title', 'Оформить заказ')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title mt-2">
            <h1>Make Order</h1>
        </div>
    </div>
</div>

<order-component :auth_user='@json($auth_user)' :auth_deliveries='@json($auth_deliveries)'></order-component>

@endsection

