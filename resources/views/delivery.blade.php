<html>
<head>
    <title>SAFE & RELIABLE DELIVERY</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!--metatextblock-->
    <meta name="description" content="Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы в Таганроге ! Успейте заказать! 8 800 200 59 10" />
    <meta name="keywords" content="Pizzaboy - Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы ! Успейте заказать! 8 800 200 59 10" />
    <link rel="canonical" href="https://pizzaboy-testing.com/delivery">
    <meta property="og:url" content="https://pizzaboy-testing.com/delivery" />
    <meta property="og:title" content="Pizzaboy - Доставка" />
    <meta property="og:description" content="Заказать пиццу в Таганроге! Невероятно быстрая доставка по городу. Выбери и оформи заказ прямо на сайте!" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="asset('images/og2.png')" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
</head>
<body>
    <div class="wrapper" id="app">
        @include('partials/_header')
        <div class="wrapper">
            <div class="container-fluid mt-4">
                <div class="row flex-center about-page">
                    <div class="col-lg-6 p-4">
                        <p class="text-center">
                            <b>
                                SAFE & RELIABLE DELIVERY
                            </b>
                        </p>
                        <p>
                            At Pizza Boy, we have over 50 years experience safely delivering food to Canadians – it’s what we’ve always done, and now we’re doing more.
                        </p>
                        <p><u>
                                <b>
                                    CLEANING AND SANITATION  All high touch surfaces are cleaned and sanitized frequently, both inside our restaurants and in our delivery vehicles.
                                </b>
                            </u>
                        </p>
                        <p class="font-weight-light">
                            COOKED HOT AND HANDS FREE  Our pizzas are baked HOT at 260˚C (500˚F) and then transferred by a long pizza paddle directly to your pizza box.                        </p>
                        <p>
                            <u>
                                <b>
                                    LOCKING TAMPER PROOF BOXES  Pizza Pizza has introduced our exclusive Tamper Proof Pizza Boxes.  After the box is closed and locked, your box cannot be opened until you break the seal and pull down the tab.
                                </b>                                </b>
                            </u>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @include('partials/_footer')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
