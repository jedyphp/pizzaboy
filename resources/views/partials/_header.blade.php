<div class="text-center header p-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 pt-5 pr-2">
                <p>
                    <big>
                        <u><a class="text-white" href="tel:+79188965442">+7-918-896-54-42</a></u>
                    </big>
                </p>
                <p>
                    <small>
                        <u>Taganrog City</u>
                    </small>
                </p>
            </div>
            <div class="col-lg-4 mt-4">
                <a href="/">
                    <img class="w-25" src="{{ asset('images/Logo.png') }}" alt="Pizza Boy - Logo">
                </a>
            </div>
            <div class="col-lg-4">

            </div>
            <div class="col-lg-12">
                <nav>
                    @if(Auth::check())
                    <a href="/user" title="Cabinet">Cabinet</a>
                    @else
                    <a href="/register" title="Register">Register</a>
                    <a href="/login" title="Login">Login</a>
                    @endif
                    <a href="/" title="Menu">Menu</a>
                    <a href="/delivery" title="Delivery">Delivery</a>
                    <a href="/about" title="About">About</a>
                    <a href="/cart" title="Cart">Cart</a>
                    <info-message></info-message>
                </nav>
            </div>
        </div>
    </div>
</div>
