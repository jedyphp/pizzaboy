@extends('layout')
@section('title', $item->name)
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Купить пиццу {{ $item->name }} в Таганроге! Выбери и оформи заказ прямо на сайте! Бесплатная доставка по городу Таганрог" />
<meta name="keywords" content="пицца Таганрог, купить пицца в Таганроге, пицца с доставкой Таганрог" />
<link rel="canonical" href="https://pizzaboy-testing.com/items/{{ $item->id }}">
@endsection
@section('facebook_meta')
<meta property="og:url" content="https://pizzaboy-testing.com/items/{{ $item->id }}" />
<meta property="og:title" content="Pizzaboy-testing.com" />
<meta property="og:description" content="Заказать {{ $item->name }} в Таганроге! Невероятно быстрая доставка по городу. Выбери и оформи заказ прямо на сайте!" />
<meta property="og:type" content="website" />
<meta property="og:image" content="{{ asset('images/og2.png') }}" />
@endsection

@section('content')
<div class="flex-center">
    <div id="details-box" class="mt-4">
        @if(count($item->photos))
        <div class="container-fluid">
            <view-details :slides="{{ $item->photos }}" :price="{{ $item->price }}" :item="{{ $item }}"></view-details>
        </div>
    </div>
    @endif
</div>
</div>

@endsection
