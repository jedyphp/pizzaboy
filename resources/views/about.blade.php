<html>
<head>
    <title>pizzaboy-testing.com - О нас</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!--metatextblock-->
    <meta name="description" content="Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы в Таганроге ! Успейте заказать! 8 800 200 59 10" />
    <meta name="keywords" content="Pizzaboy - Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы ! Успейте заказать! 8 800 200 59 10" />
    <link rel="canonical" href="https://pizzaboy-testing.com/about">
    <meta property="og:url" content="https://pizzaboy-testing.com/about" />
    <meta property="og:title" content="pizzaboy-testing.com - О нас" />
    <meta property="og:description" content="Заказать пиццу в Таганроге! Невероятно быстрая доставка по городу. Выбери и оформи заказ прямо на сайте!" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="asset('images/og2.png')" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
</head>
<body>
    <div class="wrapper" id="app">
        @include('partials/_header')
        <div class="wrapper">
            <div class="container-fluid mt-4">
                <div class="row flex-center about-page">
                    <div class="col-lg-6 p-4 text-center">
                        <img class="col-lg-6 p-4 rounded" src="{{ asset('images/profile.jpeg')}}" alt="Georgy Zhe">
                        <p class="text-center">
                                Hi! It's a demo version of a service site for Pizzas delivery made by <a href="https://www.linkedin.com/in/%D0%B5%D0%B3%D0%BE%D1%80-%D0%B6%D0%B5-ba1997180/">@jedyphp</a>
                                If you wanna continue, just contact me via <b>Telegram</b> @jedyphp
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @include('partials/_footer')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
