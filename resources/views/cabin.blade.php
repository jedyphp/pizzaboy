@extends('layout')
@section('title', 'Pizzaboy - Личный кабинет')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title mt-2">
            <h1>Personal Cabinet</h1>
        </div>

    </div>
</div>

<cabin-component :orders="{{ $orders }}" :user="{{ $user }}"></cabin-component>
@endsection

