@extends('layout')
@section('title', 'Pizzaboy - Корзина')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы в Таганроге ! Успейте заказать! 8 800 200 59 10" />
<meta name="keywords" content="Pizzaboy - Заказ новых комбо от 499 рублей. Сет на двоих 699 рублей. Заказать пиццу прямо сейчас. Бесплатная доставка пиццы ! Успейте заказать! 8 800 200 59 10" />
<link rel="canonical" href="https://pizzaboy-testing.com/cart">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title mt-2">
            <h1>Cart</h1>
        </div>

    </div>
</div>

<cart-component></cart-component>
@endsection
